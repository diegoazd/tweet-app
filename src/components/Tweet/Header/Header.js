import React, { Component } from 'react';
import './Header.css';

class Header extends Component {
  render() {
    return(
      <header className="tweet__header">
        <h3 className="tweet__title">
          Compose new tweet
        </h3>
      </header>
    );
  }
}

export default Header;