import React, { Component } from 'react';
import Header from './Header/Header';
import Body from './Body/Body';
import './Tweet.css';

class Tweet extends Component {
    render() {
      return (
        <div className="tweet">
          <Header/>
          <Body/>
        </div>
      );
    }
}

export default Tweet;