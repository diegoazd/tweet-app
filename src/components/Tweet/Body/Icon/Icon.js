import React, { Component } from 'react';
import PropTypes from 'prop-types';

const icons = {
  image: 'fa fa-picture-o',
  video: 'fa fa-video-camera',
  marker: 'fa fa-map-marker',
  audio: 'fa fa-volume-down',
  default: "fa fa-ban"
};

class Icon extends Component {
  getIcon(name) {
    return icons[name];
  }
  render() {
    return(
      <button>
        <i className={ this.getIcon(this.props.name) }></i>
      </button>
    );
  }
}

Icon.defaultProps = {
  name: "default"
}

Icon.propTypes = {
  name:  PropTypes.string.isRequired
}

export default Icon;