import React, { Component } from 'react';
import Icon from './Icon/Icon';
import './Body.css';

const buttons = [
  {
    id: 1,
    name: "video"
  },
  {
    id: 2,
    name: "audio"
  },
  {
    id: 3,
    name: "image"
  },
  {
    id: 4,
    name: "marker"
  },
]
class Body extends Component {
    render() {
      return (
        <div className="grid tweet__body">
          <div>
            <img className="tweet__avatar" alt="" src="https://images-na.ssl-images-amazon.com/images/M/MV5BMjUzZTJmZDItODRjYS00ZGRhLTg2NWQtOGE0YjJhNWVlMjNjXkEyXkFqcGdeQXVyMTg4NDI0NDM@._V1_UY256_CR42,0,172,256_AL_.jpg"/>
          </div>
          <div>
            <textarea placeholder="Ola que ase..." className="tweet__txt"></textarea>
            {buttons.map((e, index) => <Icon key={  `icon-${index}` } name={e.name}/>)}
            <Icon/>
          </div>
        </div>
      );
    }
}

export default Body;