import React from 'react';
import ReactDOM from 'react-dom';
import Tweet from './components/Tweet/Tweet';
import './index.css';

ReactDOM.render(<Tweet />, document.getElementById('root'));
